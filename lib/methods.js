import { Meteor } from 'meteor/meteor';
import { Chats, Messages } from '../lib/collections';

Meteor.methods({

    newMessage(message) {
        check(message, {
            type: String,
            text: String,
            chatId: String
        });

        message.timestamp = new Date();
        const messageId = Messages.insert(message);
        Chats.update(message.chatId, { $set: { lastMessage: message } });

        return messageId;
    },

    updateName(name) {
        if (!this.userId) {
            throw new Meteor.Error('not-logged-in', 'Must be logged in to update name');
        }

        check(name, String);

        if (name.length === 0) {
            throw new Meteor.Error('not-logged-in', 'Must be logged in to update name');
        }

        return Meteor.users.update(this.userId, { $set: { 'profile.name': name } });
    },

    newChat(otherUserId) {
        if (!this.userId) {
            throw new Meteor.Error(
                'not-logged-in',
                'Must be logged in to create chat'
            );
        }

        check(otherUserId, String);
        const otherUser = Meteor.users.findOne(otherUserId);

        if (!otherUser) {
            throw new Meteor.Error(
                'user-not-exists',
                'Other user doesn\'t exist'
            );
        }

        const chat = {
            userIds: [ this.userId, otherUserId ],
            createdAt: new Date()
        }

        const chatId = Chats.insert(chat);
        return chatId;
    },

    removeChat(chatId) {
        if (!this.userId) {
            throw new Meteor.Error(
                'not-logged-in',
                'Must be logged in to create chat'
            );
        }

        check(chatId, String);
        const chat = Chats.findOne(chatId);

        if (!chat || !_.include(chat.userIds, this.userId)) {
            throw new Meteor.Error(
                'remove-chat',
                'Chat doesn\'t exist or isn\'t yours'
            );
        }

        Messages.remove({ chatId: chatId });
        return Chats.remove({ _id: chatId });
    },

});
